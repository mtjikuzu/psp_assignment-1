package edu.luc.psp1.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.luc.psp1.Magistrate;
import edu.luc.psp1.Person;

public class MagistrateTest {
	Magistrate PastorTjikuzu = new Magistrate();
	@Test
	public void testMarriage() {		
		Person Bride = new Person("Kelly Clarkson", 25, 'F', 'S');
		Person Groom = new Person("Jason Mraz", 22, 'M', 'S');
		PastorTjikuzu.Marriage(Bride, Groom);
		assertTrue(PastorTjikuzu.isMarried());		
	}

	@Test
	public void testAnnulment() {
		Person wife = new Person("Kelly Clarkson", 25, 'F', 'M');
		Person husband = new Person("Jason Mraz", 22, 'M', 'M');
		PastorTjikuzu.Annulment(wife, husband, false);
		assertTrue(PastorTjikuzu.isAnnulled());
	}

	@Test
	public void testDivorce() {
		Person wife = new Person("Kelly Clarkson", 25, 'F', 'M');
		Person husband = new Person("Jason Mraz", 22, 'M', 'M');
		PastorTjikuzu.Divorce(wife, husband);
		assertTrue(PastorTjikuzu.isDivorced()) ;
	}

}
