package edu.luc.psp1;

public class Magistrate {
	private boolean isMarried = false;
	private boolean isAnnulled = false;
	private boolean isDivorced = false;

	public void Marriage(Person Bride, Person Groom) {

		// Check to see if both lovebirds are over 18
		if ((Bride.getAge() >= 18) && (Groom.getAge() >= 18)) {
			// Check to if both lovebirds are single
			if ((Bride.getStatus() == 'S') && (Groom.getStatus() == 'S')) {
				isMarried = true;
			}
		}
	}

	public void Annulment(Person wife, Person husband, boolean consumated) {

		// Check to see if they really are married
		if ((wife.getStatus() == 'M') && husband.getStatus() == 'M') {
			// Has the marriage been consumated
			if (!consumated) {
				isAnnulled = true;
			}
		}
	}

	public void Divorce(Person wife, Person husband) {
		// Check to see if they really are married
		if ((wife.getStatus() == 'M') && husband.getStatus() == 'M') {
			isDivorced = true;
		}
	}

	/**
	 * @return the isMarried
	 */
	public boolean isMarried() {
		return isMarried;
	}

	/**
	 * @return the isAnnulled
	 */
	public boolean isAnnulled() {
		return isAnnulled;
	}

	/**
	 * @return the isDivorced
	 */
	public boolean isDivorced() {
		return isDivorced;
	}
}
