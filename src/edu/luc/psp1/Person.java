package edu.luc.psp1;

public class Person {
	
	private String name;
	private int age;
	private char gender;
	private char status;
	
	public Person(String name, int age, char gender, char status) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.status =status;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return the gender
	 */
	public char getGender() {
		return gender;
	}
	/**
	 * @return the status
	 */
	public char getStatus() {
		return status;
	}	
}
